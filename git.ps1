Write-Host "2a"
Write-Host "  git init"

Write-Host "2b"
Write-Host "  New-Item .\git.ps1"
Write-Host "  git add -A && git commit -m `"inital commit`""
Write-Host "  ... making changes to git.ps1 ..."
Write-Host "  git commit .\git.ps1 -m `"Write task result to the console`""

Write-Host "2c"
Write-Host "
  Anders als beim einem gerichteten Graphen, zeigen die Kanten bei einem 
  gerichteten azyklischen Graphen (DAG) immer in einen Richtung, beginnend bei der Wurzel (HEAD)
  bis hin zu den Blaettern (COMMITS).

  Ein DAG eignet sich gut fuer eine Versionsverwahltung da es uns erlaubt,
  Immutable Persitent Datenstrukturen zu verwenden, aendern wir dateien in der Versionsverwahltung
  so muessen wir nur die geaenderten nodes (FILES) 'kopieren' und lassen den HEAD Pointer
  auf den neuen COMMIT zeigen. Dadurch sind wir in der Lage meherer Versionen des
  Quellcodes zu bekommen ohne jedes mal alle Files zu kopieren, was der fall waere, wenn wir jedes
  mal, wenn wir eine commit machen, das ganze Verzeichnis zipen wuerden. 
"

Write-Host "2d"
Write-Host "git log -n 1"

Write-Host "2e"
Write-Host "
  .gitignore
    **/*.log
    **/*.tmp
    tmp/
"

Write-Host "2f"
Write-Host "git clone /path/to/git/repository cloned_repo"
Write-Host "git checkout -b new-branch"

Write-Host "2i"
Write-Host "Make some changes"

Write-Host "make some chnages in cloned_repo..."
Write-Host "in /path/to/git/repository : git pull /path/to/cloned_repo"
Write-Host "resolve merge conflicts"
Write-Host "git commit -a -m `"nice commit message`""